const data = {
	products:[
	{
		_id: '1',
		name: 'Wonka Nerds Big Chewy Candy 120g',
		category : 'Candies',
		image: '/images/p1.jpg',
		price: 150,
		countInStock: 10,
		brand : 'Nerds',
		rating: 3,
		numReviews: 50,
		description: 'Big Chewy Nerds are a modern twist on a candy classic.',
	},

	{
		_id: '2',
		name: 'Toblerone Giant 12.6-Ounce Chocolate Bar',
		category : 'Chocolates',
		image: '/images/p2.jpg',
		price: 200,
		countInStock: 20,
		brand : 'Toblerone',
		rating: 4.5,
		numReviews: 100,
		description: 'A very tasty chocolate',
	},

	{
		_id: '3',
		name: 'Butterfinger Candy Bars: 36-Piece Box',
		category : 'Chocolates',
		image: '/images/p3.jpg',
		price: 200,
		countInStock: 100,
		brand : 'Butterfinger',
		rating: 4.5,
		numReviews: 80,
		description: 'A very tasty chocolate',
	},

	{
		_id: '4',
		name: 'Triangular Shaped Candy',
		category : 'Candies',
		image: '/images/p4.jpg',
		price: 100,
		countInStock: 10,
		brand : 'Candy',
		rating: 4,
		numReviews: 20,
		description: 'A very tasty chocolate',
	},

	{
		_id: '5',
		name: 'Leone Rose Candy Drops: 5-Ounce Tin',
		category : 'Candies',
		image: '/images/p5.jpg',
		price: 500,
		countInStock: 0,
		brand : 'Leone Rose',
		rating: 4.5,
		numReviews: 10,
		description: 'A very tasty candy',
	},

	{
		_id: '6',
		name: 'Archie McPhee Cupcake Mints: 1-Ounce Tin',
		category : 'Candies',
		image: '/images/p6.jpg',
		price: 250,
		countInStock: 10,
		brand : 'Archie McPhee',
		rating: 5.0,
		numReviews: 60,
		description: 'A very tasty candy',
	},

	],
};

export default data;